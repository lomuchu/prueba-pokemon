# prueba-pokemon

Prueba de la API de Pokemon.

Basado en los servicios de https://pokeapi.co/ realizar un “sistema de administración personal de pokemons”

 ## Configuraciones

**Configuracion de la base de datos**

Primero nos posicionamos en el archivo:

> src\main\resources\application.proprerties

En la siguiente fragemento podemos configurar la url de nuestra conexion a la base de datos, agregar el usuario y contraseña, tambien puede ocupar el que ya esta por defecto ya que es una base remota de pruebas

        #MySQL configuracion
    spring.datasource.url =
    spring.datasource.username = 
    spring.datasource.password = 

## Configuracion del usuario 
Para configurar el usuario y contraseña del usuario propietario del Oauth Server primero nos posicionamos en el archivo:

> src\main\resources\application.proprerties

Y modificamos las siguientes lineas

    #configuracion del usuario propietario de Oauth
    oaut.owner.user=
    oaut.owner.password=

No es lo recomendable pero por falta de tiempo ya no pude mejorarlo

## Scripts de la base de datos

Los scripts se encuentran en la siguiente ruta

> src\main\resources\scripts\bd\script_bd.sql

## Ejecucion del proyecto

Nos posicionamos a la altura del "src" y ejecutamos lo siguiente

**compilacion**

    mvn clean install

**ejecucion**

    mvn spring-boot:run

**ejecucion del jar**
Ingresamos a la carpeta "target" y ejecutamos lo siguiente

    java -jar pokemons-0.0.1-SNAPSHOT.jar

## Prueba del API
**Documentacion de las API's**

[http://localhost:8080/docapi/index.html#/user](http://localhost:8080/docapi/index.html#/user)

**Documentacion Postman**
[https://documenter.getpostman.com/view/4508795/T17KenKc?version=latest](https://documenter.getpostman.com/view/4508795/T17KenKc?version=latest)

**Colleccion Postman**
[https://www.postman.com/collections/59b853e880fc5f475d13](https://www.postman.com/collections/59b853e880fc5f475d13)