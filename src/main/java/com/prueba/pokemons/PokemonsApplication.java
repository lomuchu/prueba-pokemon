package com.prueba.pokemons;

import com.mangofactory.swagger.plugin.EnableSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
@EnableSwagger
public class PokemonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokemonsApplication.class, args);
	}

}
