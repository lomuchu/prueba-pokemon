package com.prueba.pokemons.controllers;

import com.prueba.pokemons.exceptions.ServiceException;
import com.prueba.pokemons.exceptions.ValidationException;
import com.prueba.pokemons.models.dto.ListUsersPokemonDTO;
import com.prueba.pokemons.models.dto.UserDTO;
import com.prueba.pokemons.models.dto.UsersPokemonDTO;
import com.prueba.pokemons.services.UserInfoService;
import com.prueba.pokemons.services.UsersPokemonService;
import com.prueba.pokemons.utils.Validator;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@Api(value = "main", description = "La API principal del usuario")
public class PokemonPrincipalController {

    @Autowired
    private UserInfoService userService;

    @Autowired
    private UsersPokemonService usersPokemonService;

    @ApiOperation(value = "Obtener los usuarios", notes = "Obtiene los usuarios como un arreglo de objetos JSON. ")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")})
    @CrossOrigin(origins = "*")
    @GetMapping("/user")
    public Object getAllUser(@RequestHeader HttpHeaders requestHeader) {
        List<UserDTO> userInfos = userService.getAllActiveUserInfo();
        if (userInfos == null || userInfos.isEmpty()) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(userInfos, HttpStatus.OK);
    }

    @ApiOperation(value = "Obtener los pokemons por usuario", notes = "Obtiene los pokemons del usuario proporcionado como un arreglo de objetos JSON. Datos obligatorios id del usuario a consultar ")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")})
    @CrossOrigin(origins = "*")
    @GetMapping("/user/pokemon")
    public Object getPokemons(@RequestBody @Valid UsersPokemonDTO user, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            Validator.validGetPokemons(user);

            ListUsersPokemonDTO lista = this.usersPokemonService.findByUser(user);

            return new ResponseEntity<>(lista, HttpStatus.OK);
        } catch (ValidationException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error validacion: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error al consultar la bd: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error general: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Agregar un pokemon a un usuario", notes = "Agrega un pokemon al usuario proporcionado. Datos obligatorios: id del usuario, nombre o numero del pokemon, un alias")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")})
    @CrossOrigin(origins = "*")
    @PostMapping("/user/pokemon")
    public ResponseEntity<Map<String, String>> postPokemons(@RequestBody @Valid UsersPokemonDTO user, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            Validator.validPostPokemons(user);
            this.usersPokemonService.save(user);
            Map<String, String> resultado = new HashMap<>();
            resultado.put("resultado", "Pokemon agregado correctamente");
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        } catch (ValidationException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error validacion: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error al consultar la bd: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error general: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Actualiza un pokemon por usuario", notes = "Actualiza un pokemon por usuario proporcionado. Datos obligatorios: id del usuario, id del pokemon y los datos a actualizar ")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")})
    @CrossOrigin(origins = "*")
    @PutMapping("/user/pokemon")
    public ResponseEntity<Map<String, String>> putPokemons(@RequestBody @Valid UsersPokemonDTO user, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            Validator.validPutPokemons(user);
            this.usersPokemonService.update(user);
            Map<String, String> resultado = new HashMap<>();
            resultado.put("resultado", "Pokemon actualizado correctamente");
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        } catch (ValidationException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error validacion: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error al consultar la bd: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error general: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Actualiza varios pokemons por usuario", notes = "Actualiza varios pokemons por usuario proporcionado. Datos obligatoris: id del usuario, lista de pokemons con sus respectivos ids y datos a acutalizar")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")})
    @CrossOrigin(origins = "*")
    @PutMapping("/user/pokemon/list")
    public ResponseEntity<Map<String, String>> putPokemonsList(@RequestBody @Valid ListUsersPokemonDTO user, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            Validator.validPutPokemonsList(user);
            this.usersPokemonService.updateList(user);
            Map<String, String> resultado = new HashMap<>();
            resultado.put("resultado", "Pokemons actualizados correctamente");
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        } catch (ValidationException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error validacion: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error al consultar la bd: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error general: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Elimina un pokemon por usuario", notes = "Elimina un pokemon por usuario proporcionado. Datos obligatorios: id del usuario y id del pokemon a eliminar")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")})
    @CrossOrigin(origins = "*")
    @DeleteMapping("/user/pokemon")
    public ResponseEntity<Map<String, String>> deletePokemons(@RequestBody @Valid UsersPokemonDTO user, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            Validator.validPutPokemons(user);
            this.usersPokemonService.delete(user);
            Map<String, String> resultado = new HashMap<>();
            resultado.put("resultado", "Pokemon eliminado correctamente");
            return new ResponseEntity<>(resultado, HttpStatus.OK);
        } catch (ValidationException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error validacion: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error al consultar la bd: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            Map<String, String> resultado = new HashMap<>();
            resultado.put("error", "error general: " + e.getMessage());
            return new ResponseEntity<>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
