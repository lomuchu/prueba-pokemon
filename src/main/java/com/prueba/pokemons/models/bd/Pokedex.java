package com.prueba.pokemons.models.bd;

import com.prueba.pokemons.models.dto.PokemonDTO;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "pokedex")
public class Pokedex implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "imagen")
    private String imagen;

    @OneToMany(mappedBy = "pokemon")
    List<UsersPokemon> userspokemon;

    public Pokedex() {
    }
    
    
    
    public Pokedex(Integer id, String nombre, Integer numero, String imagen, List<UsersPokemon> userspokemon) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
        this.userspokemon = userspokemon;
    }
    
    public Pokedex(Integer id, String nombre, Integer numero, String imagen) {
        this(id, nombre, numero, imagen, null);
    }
}
