package com.prueba.pokemons.models.bd;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Type;

@Entity
@Data
@Table(name = "userspokemon")
public class UsersPokemon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "alias")
    private String alias;

    @Column(name = "nature")
    private String nature;

    @Column(name = "shiny")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean shiny;

    @ManyToOne
    @JoinColumn(name = "user")
    UserInfo user;

    @ManyToOne
    @JoinColumn(name = "pokemon")
    Pokedex pokemon;

    public UsersPokemon() {
    }

    
    
    public UsersPokemon(Integer id, String alias, String nature, Boolean shiny, UserInfo user, Pokedex pokemon) {
        this.id = id;
        this.alias = alias;
        this.nature = nature;
        this.shiny = shiny;
        this.user = user;
        this.pokemon = pokemon;
    }
    
    
}
