package com.prueba.pokemons.models.bd;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import lombok.Data;

@Entity
@Data
@Table(name = "users")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    @Column(name = "enabled")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean enabled;
    
    /*@OneToMany(mappedBy = "user")
    List<UsersPokemon> userspokemon;

    public UserInfo(Integer id, String userName, String password, String role, Boolean enabled, List<UsersPokemon> userspokemon) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
        this.userspokemon = userspokemon;
    }*/

    public UserInfo() {
    }
    
    
    
    public UserInfo(Integer id, String userName, String password, String role, Boolean enabled) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
    }

}
