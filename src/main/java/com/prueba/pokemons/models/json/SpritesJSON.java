package com.prueba.pokemons.models.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SpritesJSON {
    
    private String front_default;
    
    private String front_shiny;
}
