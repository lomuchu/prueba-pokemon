package com.prueba.pokemons.models.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PokemonJSON {

    private Long id;

    private String name;

    private SpritesJSON sprites;
}
