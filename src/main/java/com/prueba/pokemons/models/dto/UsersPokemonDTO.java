package com.prueba.pokemons.models.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class UsersPokemonDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UserDTO user;

    private UserPokemonDTO pokemon;

    public UsersPokemonDTO() {
    }
    
    

    public UsersPokemonDTO(UserDTO user, UserPokemonDTO pokemon) {
        this.user = user;
        this.pokemon = pokemon;
    }

}
