package com.prueba.pokemons.models.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class ListUsersPokemonDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private UserDTO user;

    private List<UserPokemonDTO> pokemons;

    public ListUsersPokemonDTO() {
    }

    public ListUsersPokemonDTO(UserDTO user, List<UserPokemonDTO> pokemons) {
        this.user = user;
        this.pokemons = new ArrayList<>(pokemons);
    }

}
