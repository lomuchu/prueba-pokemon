
package com.prueba.pokemons.models.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class UserPokemonDTO implements Serializable{
    
     private static final long serialVersionUID = 1L;
     
     private Integer id;
    
    private String nombre;
    
    private Integer numero;
    
    private String imagen;
    
    private String alias;
    
    private String nature;
    
    private Boolean shiny;

    public UserPokemonDTO() {
    }
    
    

    public UserPokemonDTO(Integer id, String nombre, Integer numero, String imagen, String alias, String nature, boolean shiny) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
        this.alias = alias;
        this.nature = nature;
        this.shiny = shiny;
    }

}
