package com.prueba.pokemons.models.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class UserDTO implements Serializable{
    
     private static final long serialVersionUID = 1L;
     
     private Integer id;
     
     private String nombre;
     
     private String rol;
     
     private Boolean enabled;

    public UserDTO() {
    }
     
     

    public UserDTO(Integer id, String nombre, String rol, Boolean enabled) {
        this.id = id;
        this.nombre = nombre;
        this.rol = rol;
        this.enabled = enabled;
    }
     
     
    
}
