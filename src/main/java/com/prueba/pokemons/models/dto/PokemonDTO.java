
package com.prueba.pokemons.models.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class PokemonDTO implements Serializable{
    
     private static final long serialVersionUID = 1L;
    
    private Integer id;
    
    private String nombre;
    
    private Integer numero;
    
    private String imagen;

    public PokemonDTO() {
    }
    
    
    
    public PokemonDTO(Integer id, String nombre, Integer numero, String imagen) {
        this.id = id;
        this.nombre = nombre;
        this.numero = numero;
        this.imagen = imagen;
    }
}