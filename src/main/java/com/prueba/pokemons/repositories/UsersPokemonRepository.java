package com.prueba.pokemons.repositories;

import com.prueba.pokemons.models.bd.UsersPokemon;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsersPokemonRepository extends JpaRepository<UsersPokemon, Integer>{
    
    @Query("SELECT u FROM UsersPokemon u WHERe u.user.id = :userId")
    List<UsersPokemon> findByUser(@Param("userId") Integer id);
    
     @Query("SELECT u FROM UsersPokemon u WHERe u.id = :id and u.user.id = :userId")
    UsersPokemon findByIdAndUser(@Param("id") Integer id, @Param("userId") Integer userId);
    
    @Query("SELECT u FROM UsersPokemon u WHERE u.id != :userPokemoId and u.pokemon.id = :pokemonId and u.user.id = :userId and u.alias = :alias")
    UsersPokemon findIfExistPokemon(@Param("pokemonId") Integer pokemonId, 
                                    @Param("userId") Integer userId, 
                                    @Param("userPokemoId") Integer userPokemoId, 
                                    @Param("alias") String alias);
}
