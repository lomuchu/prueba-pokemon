package com.prueba.pokemons.repositories;

import com.prueba.pokemons.models.bd.Pokedex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PokemonRepository extends JpaRepository<Pokedex, Integer>{
    
    @Query("SELECT p FROM Pokedex p WHERE p.nombre = :nombre or p.numero = :numero ")
    Pokedex findByNombreOrNumero(@Param("nombre") String nombre, @Param("numero") Integer numero);
}
