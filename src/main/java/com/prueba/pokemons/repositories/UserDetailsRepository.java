package com.prueba.pokemons.repositories;

import com.prueba.pokemons.models.bd.UserInfo;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailsRepository extends CrudRepository<UserInfo, String> {

    UserInfo findByUserNameAndEnabled(String userName, boolean enabled);

    List<UserInfo> findAllByEnabled(boolean enabled);

    UserInfo findById(Integer id);

    void deleteById(Integer id);
}
