/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.pokemons.utils;

import com.prueba.pokemons.exceptions.ValidationException;
import com.prueba.pokemons.models.dto.ListUsersPokemonDTO;
import com.prueba.pokemons.models.dto.UserDTO;
import com.prueba.pokemons.models.dto.UserPokemonDTO;
import com.prueba.pokemons.models.dto.UsersPokemonDTO;

/**
 *
 * @author Quarksoft
 */
public class Validator {

    private static void validUser(UserDTO user) throws ValidationException {
        if (user == null) {
            throw new ValidationException("No ha agregado un objeto user al objeto JSON");
        } else if (user.getId() == null || user.getId() <= 0) {
            throw new ValidationException("Agrege un id de usuario valido al objeto JSON");
        }
    }

    public static void validGetPokemons(UsersPokemonDTO user) throws ValidationException {
        if (user == null) {
            throw new ValidationException("No se ha mandado ningun objeto JSON");
        }
        validUser(user.getUser());
    }

    public static void validPostPokemons(UsersPokemonDTO user) throws ValidationException {
        validGetPokemons(user);
        if (user.getPokemon() == null) {
            throw new ValidationException("Agrege un objeto pokemon al objeto JSON");
        } else if (ObjectUtil.isEmpty(user.getPokemon().getNombre())
                && (user.getPokemon().getNumero() == null || user.getPokemon().getNumero() < 1)) {
            throw new ValidationException("Agrege un nombre o numero de pokemon valido al objeto JSON");
        } else if (ObjectUtil.isEmpty(user.getPokemon().getAlias())) {
            throw new ValidationException("Agrege un alias valido al objeto JSON");
        }
    }

    public static void validPutPokemons(UsersPokemonDTO user) throws ValidationException {
        validGetPokemons(user);
        if (user.getPokemon() == null) {
            throw new ValidationException("Agrege un objeto pokemon al objeto JSON");
        } else if (user.getPokemon().getId() == null || user.getPokemon().getId() <= 0) {
            throw new ValidationException("Agrege un id de pokemon valido al objeto JSON");
        }
    }

    public static void validPutPokemonsList(ListUsersPokemonDTO user) throws ValidationException {
        if (user == null) {
            throw new ValidationException("Agrege una lista de pokemons al objeto JSON");
        }
        validUser(user.getUser());
        if (user.getPokemons() == null || user.getPokemons().size() <= 0) {
            throw new ValidationException("Agrege una lista de pokemons al objeto JSON");
        }
        int count = 0;
        for (UserPokemonDTO pokemon : user.getPokemons()) {
            if (pokemon == null) {
                throw new ValidationException("Agrege un objeto pokemon en la posicion " + count + " del arreglo de pokemons");
            } else if (pokemon.getId() == null || pokemon.getId() <= 0) {
                throw new ValidationException("Agrege un id de pokemon en la posicion " + count + " del arreglo de pokemons");
            }
            count++;
        }
    }
}
