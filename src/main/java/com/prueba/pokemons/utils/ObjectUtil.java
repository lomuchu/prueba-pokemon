package com.prueba.pokemons.utils;

public class ObjectUtil {
    
    public static boolean isEmpty(String cadena) {
        if(cadena != null && !cadena.isEmpty()) {
            return false;
        } 
        return true;
    }
    
    public static boolean isValidInt(Integer numero) {
        if(numero != null && numero > 0) {
            return true;
        } 
        return false;
    }
}
