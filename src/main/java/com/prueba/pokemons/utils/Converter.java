package com.prueba.pokemons.utils;

import com.prueba.pokemons.models.bd.Pokedex;
import com.prueba.pokemons.models.bd.UserInfo;
import com.prueba.pokemons.models.bd.UsersPokemon;
import com.prueba.pokemons.models.dto.PokemonDTO;
import com.prueba.pokemons.models.dto.UserDTO;
import com.prueba.pokemons.models.dto.UserPokemonDTO;
import com.prueba.pokemons.models.dto.UsersPokemonDTO;
import com.prueba.pokemons.models.json.PokemonJSON;

public class Converter {

    public static Object convertObjectPokemon(Object object) {
        if (object != null) {
            if (object instanceof PokemonDTO) {
                PokemonDTO pokemon = (PokemonDTO) object;
                return new Pokedex(pokemon.getId(), pokemon.getNombre(), pokemon.getNumero(), pokemon.getImagen());
            } else if (object instanceof Pokedex) {
                Pokedex pokedex = (Pokedex) object;
                return new PokemonDTO(pokedex.getId(), pokedex.getNombre(), pokedex.getNumero(), pokedex.getImagen());
            } else if (object instanceof PokemonJSON) {
                PokemonJSON pokemon = (PokemonJSON) object;
                return new PokemonDTO(null, pokemon.getName(), pokemon.getId().intValue(), pokemon.getSprites().getFront_default());
            }
        }
        return null;
    }

    public static Object convertObjectUser(Object object) {
        if (object != null) {
            if (object instanceof UserDTO) {
                UserDTO user = (UserDTO) object;
                return new UserInfo(user.getId(), user.getNombre(), null, user.getRol(), user.getEnabled());
            } else if (object instanceof UserInfo) {
                UserInfo user = (UserInfo) object;
                return new UserDTO(user.getId(), user.getUserName(), user.getRole(), user.getEnabled());
            }
        }
        return null;
    }

    public static Object convertObjectUserPokemon(Object object) {
        if (object != null) {
            if (object instanceof UsersPokemonDTO) {
                UsersPokemonDTO userPokemon = (UsersPokemonDTO) object;
                UserPokemonDTO pokemon = userPokemon.getPokemon();
                return new UsersPokemon(null, pokemon.getAlias(),
                        pokemon.getNature(),
                        pokemon.getShiny(),
                        (UserInfo) convertObjectUser(userPokemon.getUser()),
                        new Pokedex(null, pokemon.getNombre(), pokemon.getNumero(), pokemon.getImagen()));
            } else if (object instanceof UsersPokemon) {
                UsersPokemon userPokemon = (UsersPokemon) object;
                Pokedex pokemon = userPokemon.getPokemon();
                return new UsersPokemonDTO((UserDTO) convertObjectUser(userPokemon.getUser()),
                        new UserPokemonDTO(userPokemon.getId(), pokemon.getNombre(), pokemon.getNumero(), pokemon.getImagen(),
                                userPokemon.getAlias(), userPokemon.getNature(), userPokemon.getShiny()));
            } else if (object instanceof UserPokemonDTO) {
                UserPokemonDTO pokemon = (UserPokemonDTO) object;
                return new PokemonDTO(null, pokemon.getNombre(), pokemon.getNumero(), pokemon.getImagen());
            }
        }
        return null;
    }
}
