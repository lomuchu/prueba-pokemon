package com.prueba.pokemons.services;

import com.prueba.pokemons.exceptions.ServiceException;
import com.prueba.pokemons.models.dto.ListUsersPokemonDTO;
import com.prueba.pokemons.models.dto.UsersPokemonDTO;
import java.util.List;


public interface UsersPokemonService {
    
    void save(UsersPokemonDTO userPokemon) throws ServiceException;
    
    ListUsersPokemonDTO findByUser(UsersPokemonDTO user) throws ServiceException;
    
    void update(UsersPokemonDTO userPokemon) throws ServiceException;
    
    void updateList(ListUsersPokemonDTO list) throws ServiceException;
    
    void delete(UsersPokemonDTO userPokemon) throws ServiceException;
}
