package com.prueba.pokemons.services;

import com.prueba.pokemons.models.bd.UserInfo;
import com.prueba.pokemons.models.dto.UserDTO;
import java.util.List;

public interface UserInfoService {

    UserInfo getUserInfoByUserName(String userName);

    List<UserDTO> getAllActiveUserInfo();

    UserInfo getUserInfoById(Integer id);

}
