package com.prueba.pokemons.services.impl;

import com.prueba.pokemons.exceptions.ServiceException;
import com.prueba.pokemons.models.bd.Pokedex;
import com.prueba.pokemons.models.bd.UserInfo;
import com.prueba.pokemons.models.bd.UsersPokemon;
import com.prueba.pokemons.models.dto.ListUsersPokemonDTO;
import com.prueba.pokemons.models.dto.PokemonDTO;
import com.prueba.pokemons.models.dto.UserDTO;
import com.prueba.pokemons.models.dto.UserPokemonDTO;
import com.prueba.pokemons.models.dto.UsersPokemonDTO;
import com.prueba.pokemons.repositories.UsersPokemonRepository;
import com.prueba.pokemons.services.PokemonService;
import com.prueba.pokemons.services.UserInfoService;
import com.prueba.pokemons.services.UsersPokemonService;
import com.prueba.pokemons.utils.Converter;
import com.prueba.pokemons.utils.ObjectUtil;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersPokemonServiceImpl implements UsersPokemonService {

    @Autowired
    private UsersPokemonRepository usersPokemonRepository;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private PokemonService pokemonService;

    @Override
    public void save(UsersPokemonDTO userPokemon) throws ServiceException {
        try {

            UserInfo user = this.userInfoService.getUserInfoById(userPokemon.getUser().getId());
            PokemonDTO pokemon = this.pokemonService.findPokemonByNombreOrNumero((PokemonDTO) Converter.convertObjectUserPokemon(userPokemon.getPokemon()));
            if (user == null) {
                throw new ServiceException("No se encontro ningun usuario con el id proporcionado");
            }
            if (pokemon == null) {
                throw new ServiceException("No se encontro ningun pokemon con el nombre o numero proporcionado");
            }
            this.verifyIfExits(pokemon.getId(), user.getId(), 0, userPokemon.getPokemon().getAlias());
            
            UserPokemonDTO pokemonDTO = userPokemon.getPokemon();
            UsersPokemon objeto = new UsersPokemon(null,
                    pokemonDTO.getAlias(),
                    pokemonDTO.getNature(),
                    pokemonDTO.getShiny(), user, (Pokedex) Converter.convertObjectPokemon(pokemon));
            this.usersPokemonRepository.save(objeto);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public ListUsersPokemonDTO findByUser(UsersPokemonDTO user) throws ServiceException {
        try {
            ListUsersPokemonDTO lista = null;
            List<UsersPokemon> result = this.usersPokemonRepository.findByUser(user.getUser().getId());
            if (result != null && result.size() > 0) {
                List<UserPokemonDTO> pokemons = new ArrayList<>();
                for (UsersPokemon pokemon : result) {
                    UserPokemonDTO aux = ((UsersPokemonDTO) Converter.convertObjectUserPokemon(pokemon)).getPokemon();
                    aux.setId(pokemon.getId());
                    pokemons.add(aux);
                }

                lista = new ListUsersPokemonDTO((UserDTO)Converter.convertObjectUser(result.get(0).getUser()), pokemons);

            } else {
                throw new ServiceException("no se encontro ningun usuario con id proporcionado");
            }

            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void update(UsersPokemonDTO userPokemonDTO) throws ServiceException {
        try {
            UsersPokemon usersPokemon = this.usersPokemonRepository.findByIdAndUser( userPokemonDTO.getPokemon().getId(), userPokemonDTO.getUser().getId());
            if (usersPokemon == null) {
                throw new ServiceException("no se encontro ningun usuario con el id de pokemon dado");
            }
            
            this.verifyIfExits(usersPokemon.getPokemon().getId(), userPokemonDTO.getUser().getId(), userPokemonDTO.getPokemon().getId(),userPokemonDTO.getPokemon().getAlias() );
            if (!ObjectUtil.isEmpty(userPokemonDTO.getPokemon().getAlias())) {
                usersPokemon.setAlias(userPokemonDTO.getPokemon().getAlias());
            }
            if (!ObjectUtil.isEmpty(userPokemonDTO.getPokemon().getNature())) {
                usersPokemon.setNature(userPokemonDTO.getPokemon().getNature());
            }
            if (userPokemonDTO.getPokemon().getShiny() != null) {
                usersPokemon.setShiny(userPokemonDTO.getPokemon().getShiny());
            }
            this.usersPokemonRepository.save(usersPokemon);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void updateList(ListUsersPokemonDTO lista) throws ServiceException {
        try {
            for (UserPokemonDTO pokemon : lista.getPokemons()) {
                this.update(new UsersPokemonDTO(lista.getUser(), pokemon));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void delete(UsersPokemonDTO userPokemonDTO) throws ServiceException {
        try {
            UsersPokemon usersPokemon = this.usersPokemonRepository.findByIdAndUser( userPokemonDTO.getPokemon().getId(), userPokemonDTO.getUser().getId());
            if (usersPokemon == null) {
                throw new ServiceException("no se encontro ningun usuario con el id de pokemon dado");
            }
            this.usersPokemonRepository.delete(usersPokemon);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }
    }
    
    private void verifyIfExits(Integer idpokemon, Integer iduser, Integer idUserPokemon, String alias) throws ServiceException{
        UsersPokemon exits = this.usersPokemonRepository.findIfExistPokemon(idpokemon, iduser, idUserPokemon, alias);
            if(exits != null){
                throw new ServiceException("No se puede agraguer un pokemon del mismo tipo con el mismo alias");
            }
    }
}
