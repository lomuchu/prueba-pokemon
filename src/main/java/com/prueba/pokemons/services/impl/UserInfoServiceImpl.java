package com.prueba.pokemons.services.impl;

import com.prueba.pokemons.models.bd.UserInfo;
import com.prueba.pokemons.models.dto.UserDTO;
import com.prueba.pokemons.repositories.UserDetailsRepository;
import com.prueba.pokemons.services.UserInfoService;
import com.prueba.pokemons.utils.Converter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserDetailsRepository userDatailsRepository;

    @Override
    public UserInfo getUserInfoByUserName(String userName) {
        boolean enabled = true;
        return userDatailsRepository.findByUserNameAndEnabled(userName, enabled);
    }

    @Override
    public List<UserDTO> getAllActiveUserInfo() {
        List<UserDTO> lista = null;
        List<UserInfo> result = userDatailsRepository.findAllByEnabled(true);
        if(result != null && result.size() > 0) {
            lista = new ArrayList<>();
            for(UserInfo user : result) {
                lista.add((UserDTO)Converter.convertObjectUser(user));
            }
        }
        return lista;
    }

    @Override
    public UserInfo getUserInfoById(Integer id) {
        return userDatailsRepository.findById(id);
    }
}
