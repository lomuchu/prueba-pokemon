package com.prueba.pokemons.services.impl;

import com.prueba.pokemons.models.bd.Pokedex;
import com.prueba.pokemons.models.dto.PokemonDTO;
import com.prueba.pokemons.repositories.PokemonRepository;
import com.prueba.pokemons.services.PokemonService;
import com.prueba.pokemons.services.RestService;
import com.prueba.pokemons.utils.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PokemonServiceImpl extends RestService implements PokemonService{

    @Autowired
    private PokemonRepository pokemonRepository;
    
    
    @Override
    @Transactional
    public PokemonDTO savePokemon(PokemonDTO pokemon) {
        Pokedex pokedex = (Pokedex) Converter.convertObjectPokemon(pokemon);
        pokedex = this.pokemonRepository.save(pokedex);
        return (PokemonDTO)Converter.convertObjectPokemon(pokedex);
    }

    @Override
    public PokemonDTO findPokemonByNombreOrNumero(PokemonDTO pokemon) {
        PokemonDTO pokemonDTO = null;
        Pokedex result = this.pokemonRepository.findByNombreOrNumero(pokemon.getNombre(), pokemon.getNumero());
        if(result == null) {
           pokemonDTO = this.getByRestTemplate(pokemon);
            if(pokemonDTO != null) {
               pokemonDTO = this.savePokemon(pokemonDTO);
            }
        }else  {
            pokemonDTO = (PokemonDTO) Converter.convertObjectPokemon(result);
        }
        return pokemonDTO;
    }
    
    
}
