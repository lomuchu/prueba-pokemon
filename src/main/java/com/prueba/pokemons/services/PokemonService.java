package com.prueba.pokemons.services;

import com.prueba.pokemons.models.dto.PokemonDTO;

public interface PokemonService {
    
    PokemonDTO savePokemon(PokemonDTO pokemon);
    
    PokemonDTO findPokemonByNombreOrNumero(PokemonDTO pokemon);
}
