package com.prueba.pokemons.services;

import com.prueba.pokemons.models.bd.Pokedex;
import com.prueba.pokemons.models.dto.PokemonDTO;
import com.prueba.pokemons.models.json.PokemonJSON;
import com.prueba.pokemons.utils.Converter;
import com.prueba.pokemons.utils.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

public class RestService {
    
    
     @Autowired
    private RestTemplate restTemplate;

    @Value("${pokemon.api.url}")
    private String urlPokemon;
    
    protected PokemonDTO getByRestTemplate(PokemonDTO pokemon) {
        String url = this.urlPokemon;
        if(!ObjectUtil.isEmpty(pokemon.getNombre())) {
            url += pokemon.getNombre();
        } else if(ObjectUtil.isValidInt(pokemon.getNumero()) ){
            url += pokemon.getNumero();
        }
       PokemonJSON pokemonJSON = this.restTemplate.getForObject(url, PokemonJSON.class);
       return  (PokemonDTO)Converter.convertObjectPokemon(pokemonJSON);
    }
}
